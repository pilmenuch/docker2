FROM nginx:latest

COPY ./nginx.conf /etc/nginx/conf.d/default.conf
VOLUME /var/www/html/

RUN apt-get update && apt-get install mc tree lsof -y

EXPOSE 8077